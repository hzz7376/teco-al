// MIT License
// 
// Copyright (c) 2024, Tecorigin Co., Ltd.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
 
#include "common/tecotest.h"

size_t tecotestGetVersion(void) {
    // TYPE {0:release, 1:release candidate, 2: beta, 3: alpha}
#define TECOTEST_VERSION_TYPE 3
#define TECOTEST_VERSION_TYPE_NO 0

    return TECOTEST_MAJOR * 100000000 + TECOTEST_MINOR * 1000000 + TECOTEST_PATCHLEVEL * 10000 +
           TECOTEST_VERSION_TYPE * 100 + TECOTEST_VERSION_TYPE_NO;

#undef TECOTEST_VERSION_TYPE
#undef TECOTEST_VERSION_TYPE_NO
}
