syntax = "proto2";

package testpt;

import "tensor.proto";
import "tecoal/common.proto";
import "tecoal/convolution.proto";

//-----------------UnaryOps---------------
enum UnaryOpsMode{
    BATCH_LOG = 0;
    BATCH_EXP = 1;
    BATCH_SQRT = 2;
    BATCH_RSQRT = 3;
    BATCH_SQUARE = 4;
    BATCH_SIN = 5;
    BATCH_COS = 6;
    BATCH_TANH = 7;
    BATCH_CEIL = 8;
    BATCH_FLOOR = 9;
    BATCH_FABS = 10;
    // with alpha
    BATCH_ADD_A = 11;
    BATCH_SUB_A = 12;
    BATCH_MUL_A = 13;
    BATCH_DIV_A = 14;
    BATCH_RDIV = 15;
    BATCH_POW = 16;
    // convert
    BATCH_S2H = 17;
    BATCH_H2S = 18;
}

enum ScatterOutReductionMode{
    SCATTEROUT_REDUCTION_NONE = 0;
    SCATTEROUT_REDUCTION_ADD  = 1;
    SCATTEROUT_REDUCTION_MULTIPLY = 2;
}

enum ScatterOutInputType{
    SCATTEROUT_INPUT_SCALAR = 0;
    SCATTEROUT_INPUT_ARRAY  = 1;
}

enum Algo{
    ALGO_0 = 0;
    ALGO_1 = 1;
    ALGO_2 = 2;
    ALGO_3 = 3;
    ALGO_4 = 4;
    ALGO_5 = 5;
    ALGO_6 = 6;
    ALGO_7 = 7;
    ALGO_8 = 8;
    ALGO_9 = 9;
    ALGO_10 = 10;
    ALGO_11 = 11;
    ALGO_12 = 12;
    ALGO_13 = 13;
    ALGO_14 = 14;
    ALGO_15 = 15;
    ALGO_16 = 16;
    ALGO_17 = 17;
    ALGO_18 = 18;
    ALGO_19 = 19;
    ALGO_20 = 20;
    ALGO_21 = 21;
    ALGO_22 = 22;
    ALGO_23 = 23;
    ALGO_24 = 24;
    ALGO_25 = 25;
    ALGO_26 = 26;
    ALGO_27 = 27;
    ALGO_28 = 28;
    ALGO_29 = 29;
    ALGO_30 = 30;
    ALGO_31 = 31;
    ALGO_32 = 32;
    ALGO_33 = 33;
    ALGO_34 = 34;
    ALGO_35 = 35;
    ALGO_36 = 36;
    ALGO_37 = 37;
    ALGO_38 = 38;
    ALGO_39 = 39;
    ALGO_40 = 40;
    ALGO_41 = 41;
    ALGO_42 = 42;
    ALGO_43 = 43;
    ALGO_44 = 44;
    ALGO_45 = 45;
    ALGO_46 = 46;
    ALGO_47 = 47;
    ALGO_48 = 48;
    ALGO_49 = 49;
    ALGO_50 = 50;
}

//scatterOut
message ScatterOutParam {
    optional int32 target_dim                               = 1;
    optional float alpha                                    = 2;
    optional ScatterOutReductionMode scatter_out_reduce_mode = 3;
    optional ScatterOutInputType scatter_out_input_type      = 4;
    optional Algo algo                                 = 5;
}

//UnaryOps
message UnaryOpsParam {
    optional HandleParam handle                     = 1;
    optional UnaryOpsMode mode                      = 2;
    optional float unary_alpha                      = 3;
    optional Algo algo                        = 4;
}

//ScaleTensor
message ScaleTensorParam {
    optional HandleParam handle                     = 1;
    optional float alpha                            = 2;
    optional Algo algo                        = 3; 
}

//ArgMax
message ArgMaxParam {
    optional HandleParam handle                     = 1;
    optional int32 axis                             = 2;
    optional Algo algo                        = 3;
}

message MaskedFillParam {
    optional HandleParam handle                         = 1;
    optional float value                                = 2;
    optional Algo algo                                  = 3;
}

enum UniqueMode{
    UNIQUE_NONE = 0;
    UNIQUE_NOT_NONE = 1;
}
message UniqueParam {
    optional HandleParam handle                         = 1;
    optional bool sorted                                = 2;
    optional bool return_inverse                        = 3;
    optional bool return_counts                         = 4;
    optional UniqueMode mode                            = 5;
    optional int32 axis                                 = 6;
    optional Algo algo                                  = 7;
}

//TecoalParam
message TecoalParam {
    optional UnaryOpsParam unary_ops_param                   = 1;
    optional ArgMaxParam arg_max_param                       = 2;
    optional ScatterOutParam scatter_out_param               = 3;
    optional MaskedFillParam masked_fill_param               = 4;
    optional ScaleTensorParam scale_tensor_param             = 5;
    optional UniqueParam unique_param                        = 6;
}
